import datetime
import os
import random
import re
import time

from pip.utils import cached_property
from slackclient import SlackClient


class ShinatamaBot(object):
    read_websocket_delay = 1
    bot_id = None
    slack_client = None
    at_bot = None
    bot_name = 'Shinatama'
    report_channel_id = 'G4BFB2FKR'
    im_channel_id = 'D68MAGESK'

    def __init__(self, bot_id, token):
        self.bot_id = bot_id
        self.at_bot = "<@" + bot_id + ">"
        self.slack_client = SlackClient(token)

    def run(self):
        if self.slack_client.rtm_connect():
            print("{} connected and running!".format(self.bot_name))
            while True:
                try:
                    command, channel = self.parse_slack_output(self.slack_client.rtm_read())
                    if command and channel:
                        self.handle_command(command, channel)
                except Exception:
                    self.post_message_to_im('I fell...')
                    self.post_message_to_im('I fell, lift me up!')
                    raise
                time.sleep(self.read_websocket_delay)
        else:
            print("Connection failed. Invalid Slack token or bot ID?")

    def handle_command(self, command, channel):
        method = self.available_commands.get(command.strip())
        if not method:
            self.post_message_to_im("You ask me. But you don\'t ask with respect. "
                                    "You don\'t offer friendship... "
                                    "Although, you can try 'list' command")
        else:
            method['handler'](channel)

    def get_list_of_commands(self, channel):
        """generate a list of available commands"""

        attachments = []
        for command, method in self.available_commands.items():
            attachments.append(
                {"text": "Say '{}' and I will {}.. {}".format(command, method['handler'].__doc__,
                                                              random.choice(['maybe..', 'or will not', ''])),
                 "color": method['severity']})

        self.post_message_to_im(":eyeroll-use-sparingly: Oh.. you forgot it again.. ok let's see:",
                                attachments=attachments)

    def do_full_clean(self, channel):
        """clean all my message for this channel"""

        history = self.get_channel_history(channel)
        for message in history:
            if message['user'] == self.bot_id:
                self.slack_client.api_call("chat.delete", channel=channel, ts=message['ts'], as_user=True)

    def prepare_report(self, channel):
        """generate a daily progress report"""
        now = datetime.datetime.now()
        oldest_time = time.mktime((now.year, now.month, now.day, 0, 0, 0, 0, 0, 0))
        history = self.get_channel_history(self.report_channel_id, inclusive=True, oldest=oldest_time)
        tasks_list = []
        i = 0
        for message in history:
            message_text = message['text'].strip()
            tasks = re.findall(r'LIMS-[0-9]+[^\n]+', message_text)
            for i, task in enumerate(tasks, start=i+1):
                tasks_list.append(
                    re.sub(r'(LIMS-[0-9]+)', r'{}) <a href="https://invitae.jira.com/browse/\1">\1</a>'.format(i), task))

        if not tasks_list:
            response = 'Sorry, I can\'t find any reports for today'
        else:
            response = "```Hello<br /><br />\nHere is Tomsk's team today " \
                       "progress report below:<br /><br />\n{}<br /><br />\nThanks```".format('<br \>\n'.join(tasks_list))

        self.post_message_to_im(response)

    def get_channel_history(self, channel, full_history=False, **kwargs):
        history = {'messages': []}
        for channel_type in ('groups', 'channels', 'im'):
            history = self.slack_client.api_call("{}.history".format(channel_type), channel=channel, **kwargs)
            if history['ok']:
                break
        if full_history:
            return history
        return [message for message in history['messages']]

    def post_message_to_im(self, message, **kwargs):
        self.post_message_to_channel(self.im_channel_id, message, **kwargs)

    def post_message_to_channel(self, channel, message, **kwargs):
        self.slack_client.api_call("chat.postMessage", channel=channel, text=message, as_user=True, **kwargs)

    def parse_slack_output(self, slack_rtm_output):
        output_list = slack_rtm_output
        for output in output_list:

            if not(output and 'text' in output):
                continue

            if self.at_bot in output['text']:
                # return text after the @ mention, whitespace removed
                return output['text'].split(self.at_bot)[1].strip().lower(), output['channel']
            elif output['channel'] == self.im_channel_id and output['user'] != self.bot_id:
                return output['text'].strip().lower(), output['channel']
        return None, None

    @cached_property
    def available_commands(self):
        return {
            'list': {
                'handler': self.get_list_of_commands,
                'severity': 'good'
            },
            'do full clean': {
                'handler': self.do_full_clean,
                'severity': 'danger'
            },
            'prepare me a mail, please': {
                'handler': self.prepare_report,
                'severity': 'good'
            },
        }


if __name__ == "__main__":
    bot = ShinatamaBot(os.environ.get("BOT_ID"), os.environ.get('SLACK_BOT_TOKEN'))
    bot.run()
